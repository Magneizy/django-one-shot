from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todolists/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todolists/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todolists/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todolists/update.html"
    fields = ["name"]
    
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todolists/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitems/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todoitems/update.html"
    fields = ["task", "due_date", "is_completed", "list"]
    
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


