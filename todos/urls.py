from django.urls import path

from todos.views import (
    TodoListListView, 
    TodoListCreateView,
    TodoItemCreateView,
    TodoListDetailView,
    TodoListDeleteView,
    TodoListUpdateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("create/", TodoListCreateView.as_view(), name="create_list"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todolist_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="create_item"),       
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todoitem_edit"),
]

